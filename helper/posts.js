import fs from "fs";
import matter from "gray-matter";
import path from "path";
import showdown from "showdown";

export const getPosts = () => {
	const sortByDate = (a, b) => {
		// return a function that work with .sort method, to sort items from newer to older.
		return new Date(b.metadata.date) - new Date(a.metadata.date);
	};

	const files = fs.readdirSync(path.join('pages/post/content'));
	// console.log(files)
	let posts = files.map(filename => {
		const slug = filename.replace('.md', '');
		const markDownWithMeta = fs.readFileSync(
			path.join('pages/post/content', filename), "utf-8"
		);
		// console.log(markDownWithMeta)
		const converter = new showdown.Converter({metadata: true});
		// const html = converter.makeHtml(markDownWithMeta)
		// const metadata = converter.getMetadata()
		const {data: metadata} = matter(markDownWithMeta);
		return {slug, metadata};

	});
	posts = posts.filter(post => post.metadata.finished === true);
	return posts.sort(sortByDate);
};
