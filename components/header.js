import Link from "next/link";
import styled from "styled-components";
import {ceruleanBlue, darkColor} from "../styles/globalVariables";


const HeaderContainer = styled.header`
  cursor: pointer;
  position: fixed;
  // background-color: ${ceruleanBlue};
  background-color: rgba(42, 82, 190, 0.55);
	//opacity: 0.85;
  color: ${darkColor};
  width: 100vw;
  top: 0;
  padding: 0.25rem;
  z-index: 10;
  height: 3rem;
  backdrop-filter: blur(15px);
  text-align: center;
`

const Header = () => {
    return (
        <HeaderContainer>
	        <Link href="/" passHref>
		        <h2>DEBLOAT US</h2>
	        </Link>
        </HeaderContainer>
    )
}

export default Header
