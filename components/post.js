import Link from "next/link";
import styled from "styled-components";
import {GlassContainer, prussianBlue} from "../styles/globalVariables";

const Container = styled.div`
  cursor: pointer;
  //background-color: rgba(0,150,0,.5);
  border-radius: 4px;
  width: 400px;
  margin: 1.5rem;

  @media (max-width: 460px) {
    width: 100vw;
  }
`
const PostDataContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 0 1rem;
  @media (min-width: 768px) {
    padding: 0;
  }

  .imageContainer {
    overflow: hidden;
    height: 200px;
    width: 100%;
  }

  img {
    object-fit: cover;
    object-position: center;
    transition: all 1s ease-in-out;
    height: 100%;
    width: 100%;
    max-height: 200px;

    &:hover {
      transform: scale(1.2, 1.2)
    }
  }

  //align-items: center;
`
const TagsContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-evenly;

`
const Tag = styled.span`
  background-color: rgba(255, 255, 255, 0.5);
  //margin: 0 0.25rem;
  border-radius: 10px;
  padding: 0 0.25rem;
  color: ${prussianBlue};
`

const H1 = styled.h2`
  height: 5rem;
  //white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: justify;
  text-justify: inter-character;
`

const Date = styled.p`
  //height: 1rem;
`

const Excerpt = styled.p`
  height: 4rem;
  overflow: hidden;
  text-overflow: ellipsis;
  text-align: justify;
`

export const PostCard = ({post}) => {
//     console.log(post)
//     metadata:
//          cover_image: "/images"
//          date: "2020-10-01"
//          excerpt: "all the troubles that I passed to get a simple blog."
//          tags: ['blog']
//          title: "How I get here."
    return (
        <Link href={`/post/${post.slug}`} passHref>
            <Container>
                <GlassContainer>
                  <PostDataContainer>
                    <div className="imageContainer">
                      <img src={post.metadata.cover_image} alt={post.slug}/>
                    </div>
                    {/*<Image src={post.metadata.cover_image} alt={post.slug} height="200" width="400"/>*/}
                    <H1>{post.metadata.title}</H1>
                    <Date>{post.metadata.date}</Date>
                    <TagsContainer>
                      {post.metadata.tags.map((tag, index) => <Tag key={index}>#{tag}</Tag>)}
                    </TagsContainer>
                    <Excerpt>{post.metadata.excerpt}</Excerpt>
                  </PostDataContainer>
                </GlassContainer>
            </Container>
        </Link>
    )

}

export default PostCard;
