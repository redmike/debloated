import styled from "styled-components";
import Link from "next/link";
import {lightColor} from "../styles/globalVariables";

const FooterContainer = styled.footer`
	position: fixed;
	bottom: 0;
	width: 100vw;
	display: flex;
	align-items: center;
	justify-content: center;
	padding: 0.25rem;
	background-color: rgba(0, 0, 0, 0.5);
	color: ${lightColor};
	backdrop-filter: blur(5px);
`
const Footer = () => {
	const year = new Date(Date.now())
	return (
		<FooterContainer>
			<span>©{year.getFullYear()}</span>&nbsp;<Link href="/about"> Debloat US </Link> &nbsp;- &nbsp;<Link href="mailto:contact@debloat.us"> Contact US</Link>
		</FooterContainer>
	);
};


export default Footer;

