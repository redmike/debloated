import Link from "next/link";
import styled from "styled-components";
import {darkPurple, lightColor} from "../styles/globalVariables";

export const Button = styled.button`
  cursor: pointer;
  background-color: ${darkPurple};
  border: 2px solid ${darkPurple};
  color: ${lightColor};
  border-radius: 3px;
  padding: 0.35rem 0.6rem;
  font-weight: 800;

  &:hover {
    color: ${darkPurple};
    background-color: ${lightColor};
  }
`
const Container = styled.div`
  padding: 1.5rem 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
`

const Banner = (props) => {
  // const lastBuildTime = new Date(Date.now());
  // console.log(builtDateStamp)
  // const {title, built} = props
  return (
    <Container>
      {/*<p>*/}
      {/*{lastBuildTime.toDateString()}*/}
      {/*{builtDateStamp}*/}
      {/*</p>*/}
      <Link href={props.clickButton} passHref>
        <Button>{props.textButton}</Button>
      </Link>
    </Container>
  );
}
export default Banner

// export const getStaticProps = async () => {
//     const lastBuiltTime = new Date(Date.now())
//     return {
//         props: {
//             builtDateStamp: lastBuiltTime.toDateString(),
//         }
//     }
// }
