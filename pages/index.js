import Link from "next/link";
import {useEffect, useState} from "react";
import styled, {keyframes} from "styled-components";
import Banner from "../components/banner";
import PostCard from "../components/post";
import {getPosts} from "../helper/posts";
import {darkColor, darkPurple, GlassContainer, MainContainer} from '../styles/globalVariables';
import {Container} from "./_app";

const glitchScan = keyframes`

`;

const Span = styled.span`
    color: ${props => props.dark ? darkColor : darkPurple};
    //animation: ;
`;
const TitleContainer = styled.div`
    font-family: "Poppins", sans-serif;
    font-weight: 600;
    margin: 0;
    line-height: 1.15;
    font-size: 4rem;
    text-align: center;

    @media (max-width: 460px) {
        font-size: 2rem;
    }

    .description {
        text-align: center;
        margin: 4rem 0;
        line-height: 1.5;
        font-size: 1.5rem;
        @media (max-width: 460px) {
            font-size: 1rem;
        }
    }
`
const PostContainer = styled.div`
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  background: url("/static/backgrounds/mesh-mimetism.png") no-repeat 50% fixed;
  background-size: cover;
  width: 100vw;
  padding: 1rem;
`
const PostHero = styled.section`
  background: url("/static/backgrounds/mesh-gradient-bluecircle.png") no-repeat 50% fixed;
  background-size: cover;
  padding: 2rem 0;
  color: ghostwhite;
  width: 100vw;

`
const Section = styled.div`
  //width: 100vw;
`
const Tag = styled.span`
  margin: 0.25rem;
  cursor: pointer;
  border-radius: 5px;
  padding: 0.25rem 0.45rem;

  &:hover {
    background-color: rgba(255, 255, 255, 0.5);
    color: ${darkColor};

    span {
      background-color: transparent;
    }
  }
`

const TagsContainer = styled.div``
const CircleTag = styled.span`
    overflow-wrap: anywhere;
    border-radius: 50%;
    min-width: 4rem;
    padding: 0.09rem 0.4rem;
    background-color: rgba(255, 255, 255, 0.5);
    color: ${darkColor};

    &:hover {
        //background-color: inherit;
    }
`

const Home = ({posts, builtDateStamp}) => {
    const button = "Let's Go!"
    const buttonClick = "/#postSection";

    let tags = {};
    posts.forEach(post => {
        post.metadata.tags.map(tag => {
            tags[tag] = (tags[tag] || 0) + 1;
        });
    });
    // console.log(posts)
    // console.log(tags)
    // console.log(Object.keys(tags).map(tag => tags[tag]))
    // console.log(window.screenY)
    const [offsetY, setOffsetY] = useState(0);
    const handleScroll = () => {
        setOffsetY(window.scrollY);
    };
    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
        return () => window.removeEventListener("scroll", handleScroll);
    }, [offsetY]);
    const [animation, setAnimation] = useState(0);


    return (
      <Container>
          <MainContainer>
              <GlassContainer style={{textAlign: "center"}}>
                  <TitleContainer>
                      <Span animat={animation}>DEBLOAT</Span>
                      &nbsp;
                      <Span dark>US</Span>
                  </TitleContainer>
                  <span>last built: {builtDateStamp}</span>
                  <Banner textButton={button} clickButton={buttonClick}/>
              </GlassContainer>
          </MainContainer>
          <PostHero id="postSection">
              <Section>
                  <h2>Tags</h2>
                  <TagsContainer>
                      {Object.keys(tags).map((tag, index) => (
                        <Link key={index} href={`/tag/${tag}`} passHref>
                            <Tag>#{tag}<CircleTag>{tags[tag]}</CircleTag></Tag>
                        </Link>
                      ))}
                  </TagsContainer>
              </Section>
          </PostHero>
          <PostContainer>
              {posts.map((post, index) => (
                <PostCard key={index} post={post}/>
              ))}
          </PostContainer>
      </Container>
    );
};


export const getStaticProps = async () => {
    const sortByDate = (a, b) => {
        // return a function that work with .sort method, to sort items from newer to older.
        return new Date(b.metadata.date) - new Date(a.metadata.date);
    }
    const lastBuiltTime = new Date(Date.now())
    const siteConfig = await import("../styles/globalVariables")
    const posts = getPosts()
    return {
        props: {
            // Get an human readable time
            builtDateStamp: lastBuiltTime.toDateString(),
            builtTimeStamp: lastBuiltTime.toTimeString(),
            posts: posts.sort(sortByDate),

        },
    };
};


export default Home;
