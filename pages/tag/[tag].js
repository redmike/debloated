import {useRouter} from "next/router";
import styled from "styled-components";
import {getPosts} from "../../helper/posts";
import PostCard from "../../components/post";
import Header from "../../components/header";

const Container = styled.div`
  background: url("/static/backgrounds/mesh-gradient-chicle.png") no-repeat 50% fixed;
  background-size: cover;
  min-height: 100vh;
`;

const Title = styled.h2`
  margin-top: 3rem;
`

const Tag = ({posts}) => {

    const router = useRouter()
    const tag = router.query.tag
    console.log(posts)

    const tagPosts = []
    for (let i = 0; i < posts.length; i++) {
        if (posts[i].metadata.tags.includes(tag)) {
            tagPosts.push(posts[i])
        }
    }
    console.log(tagPosts)
    return (
        <Container>
            <Header/>
            <Title>posts for #{tag}</Title>
            {tagPosts.map((post, index) => (
                <PostCard key={index} post={post}/>
            ))}
        </Container>
    )
}

export default Tag;

export const getStaticProps = async () => {
    const posts = getPosts()
    return {
        props: {
            posts: posts
        }
    }
}

// export const getStaticProps = async () => {
//     const sortByDate = (a, b) => {
//         return new Date(b.metadata.date) - new Date(a.metadata.date)
//     }
//     const files = fs.readdirSync(path.join('pages/post/content'))
//     const posts = files.map(filename => {
//         const slug = filename.replace('.md', '')
//         const markDownWithMeta = fs.readFileSync(
//             path.join('pages/post/content', filename), "utf-8"
//         )
//         // console.log(markDownWithMeta)
//         const converter = new showdown.Converter({metadata:true})
//         // const html = converter.makeHtml(markDownWithMeta)
//         // const metadata = converter.getMetadata()
//         const {data: metadata} = matter(markDownWithMeta)
//         // console.log(metadata)
//         return {
//             slug,
//             metadata
//             // showdowndata
//         }
//     })
//     return {
//         props: {
//             posts: posts.sort(sortByDate)
//         }
//     }
// }


export const getStaticPaths = async () => {
    const posts = getPosts()
    const mess_paths = posts.map(post => (
        post.metadata.tags.map(tag => ({
                params: {tag: tag}
            })
        )
    ))
    let paths = []
    for (let i = 0; i < mess_paths.length; i++) {
        for (let j = 0; j < mess_paths[i].length; j++) {
            paths.push(mess_paths[i][j])
        }
    }
    // console.log(paths)
    return {
        paths,
        fallback: false
    }
}
