import Head from "next/head"
import styled from "styled-components";
import {GlassContainer, MainContainer, pageTitle, prussianBlue} from "../styles/globalVariables";
import * as fs from "fs";
import * as path from "path";
import matter from "gray-matter";
import {marked} from "marked";
import Header from "../components/header";
import Image from "next/image";

const AboutContainer = styled.div`
	color: ${prussianBlue};
	padding: 2rem;
	max-width: 1200px;
`;
const About = ({about}) => {
	return (
		<MainContainer style={{background:"url(/static/backgrounds/zelva.jpg) fixed 50% no-repeat", backgroundSize:"cover" }}>
			<Head>
				<title>{pageTitle} - About</title>
			</Head>
			<Header>DEBLOAT US</Header>

			<GlassContainer style={{margin: "2.5rem 1rem", maxWidth: "1200px"}}>
				<AboutContainer>
					<div dangerouslySetInnerHTML={{__html: marked(about)}}/>
				</AboutContainer>
			</GlassContainer>
		</MainContainer>
	)
};

export default About;

export const getStaticProps = async () => {
	const markDownWithMeta = fs.readFileSync(path.join('pages/about.md'), "utf-8")
	const {content: aboutData} = matter(markDownWithMeta)
	return {
		props: {
			about: aboutData
		}
	}
}