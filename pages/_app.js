import Footer from '../components/footer.js';
import '../styles/globals.css';
import Head from "next/head";
import styled from "styled-components";

export const Container = styled.div`
	//width: 100vw;
	padding: 0;
	margin: 0;
  overflow-x:hidden
`

function MyApp({Component, pageProps}) {
	return (
		<Container>
			<Head>
				<title>Debloat Us</title>
			</Head>
			<Component {...pageProps} />
			<Footer/>
		</Container>
	);
}

export default MyApp;
