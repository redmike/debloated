---

title: "The 6 Pillars of the AWS Well-Architected Framework"
date: "2023-Mar-12"
excerpt: "Core concepts, use them as checklist."
cover_image: "https://d2908q01vomqb2.cloudfront.net/77de68daecd823babbb58edb1c8e14d7106e83bb/2018/01/04/DevOps-300x150.jpg"
tags: ["aws"]
author: "Miguel R."
finished: true
---

# Six pillars of the AWS well-Architected Framework

## 1 Operational Excellence

- Ability of an organization to run workloads effectively to deliver business values.
- Perform ==operations as Code==, automate operations, eliminate the human error factor
  from
  the
  equation.
- make frequent, reversible and small changes, especially for large complicated features.
- Refine operation procedures frequently
- Anticipate failure, do not wait for it (chaos monkey in Netflix)
- Learn from all operation failures

## 2 Security

- Implement a strong identity foundation. Ensure *least privilege* and separate duties.
- Enable traceability (CloudTrail)
- Apply security in all layers (network, load balancing, every compute instance)
- Automate security best practices as much as possible
- Protect data in transit and at rest, with encryption, tokenization and access control.
- Keep people away from data, don’t manually manipulate it.
- Prepare for security events, enhanced deletion, investigating and recovery systems

## 3 Reliability

> Every workload is intended to perform a given function.

- Automatically recovers from failure, build a self-healing infrastructure.
- Test recovery procedures, simulates and see how your workload fails, also can validate recovery process.
- Scale horizontally to increase aggregate workload availability, Distribute load among many small resources rather
  using one big, ensures no single point of failure
- Stop guessing capacity, automate the addition or removal of resources depending on loads.
- Managing change in automation, to ensure consistency.

## 4 Performance Efficiency

- Democratize advanced technologies, lets AWS worry about AI, Machine Learning, computer vision, media transcoding,
  while you worry about how you can consume them,
- Use serverless architectures, remove the need to maintain servers using pre built services for databases,
  authentication, analytics etc…
- Experiment more often with virtualization, testing new thing are easy and cheap, create and delete fast.
- Consider mechanical sympathy, like no storing unstructured data in a SQL database.

## 5 Cost Optimization

> Ability to run systems to deliver business value at the lowest cost possible.

- Implement cloud financial management, teach to the employees this area.
- Adopt a consumption model, pay only for what you consume.
- Measure overall efficiency, the cost of running a resource varies with what it delivers
- Stop spending on undifferentiated heavy lifting,
- Analyze and attribute expenditure, cloud makes it easy to identify cost and usage of workloads, therefore we can
  easily know how much each resource is consuming. Optimize resources that are deemed to be incurring more cost and save
  cost consequently.

## 6 Sustainability

> Long term environmental, economic, and social impact the business activities.

- Understand your impact.
- Establish sustainability goals.
- Maximize utilization.
- Anticipate and adopt new, more efficient hardware and software offerings.
- Use managed services.
- Reduce the downstream impact of your cloud workloads.
