---
title: "How I get here."
date: "2021-jan-01"
excerpt: "all the troubles that I passed to get a simple blog."
cover_image: "/static/postimages/sucess_path.jpg"
tags: ["blog"]
finished: true
---

Well, I used that image (it's very popular I know), because really describe what is going on now, I'm not saying that I reached my goal, actually I'm just before the water, and I'm still learning to swim.
But anyway, with commitment and personality you go further that you can imagine, so let's GO!.
We're going deeper in knowledge. We're building an awesome Next.js blog using Markdown.
