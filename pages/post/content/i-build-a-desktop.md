---
title: "I built a Desktop."
date: "2022-Feb-24"
excerpt: "Emergency plan, I rush to order the parts and build Desktop."
cover_image: "/static/postimages/desktop/main.jpg"
tags: ["void", "desktop"]
author: "Miguel R."
finished: true
---

My main machine was a **Thinkpad X1C7**, using it in a dock with two wqhd (traditionally named 2k)  monitors.
I was *happily coding* and learning react native, when suddenly my computer just turn off, the first thing in my min was, shit I didn't connect the AC cable to the laptop (so it turned of for battery), so I just ensure that all the cables are well connected, so I can continue the next day. The issue came when the next day my computer didn't turn on, so the first thing in my mind, it died as a warrior doing more tasks than they can handle, so from that minute I started to "research" about building a desktop, always focused more in linux hardware.

Also, it's the worst time to build something, we are close to get the new AMD chip, that means Intel is the more recent, means expensive. GPU overpriced, all the crypto miners bought them. DDR5 uuuultra expensive.
But the good thing, if I like a desktop just for coding I don't require the bloody tech edge.

I changed from artix to void, why? I don't know, I like continue learning more

### Where I failed?
I didn't know, and also I didn't think right about the PSU, I just focus in the power, and not in the fitting, so when the PSU came, it didn't fit in the case. Then I learn there are some measurements like SFX SFX-L, what a idiot, so I'm returning it.
PROTIP: if it's your first time, buy from where you can easily return or change items.

### What I learned?
1. It's a combination of everything, but if I put everything in order, I'll say that you choose the target of your build, then which motherboard will be suitable for that, then a case that support your motherboard, read the motherboard manual you will find important information about sizing, like the GPU PSU AIO etc...
2. Void linux, I'm really enjoying my system right now, I don't have any drawback with artix, actually works well, but I think I like void more because is developed more to the stability side than the tech edge side, so I'm going to break my system less often, because it will happen anyways, ahahahaha.
3. Having everything supported with git and in a server makes the process wayyyyy lessssss painful.

You can use pcpartpicker and see if there are big incompatibility, but you should see the manual of the case and the manual of the motherboard and work from there.


Motherboard is the more important thing thinking in **further upgrades**. Buying a good one will surprise you with things that you even tought about it, in my case, I was looking for headphones, for the computer, so features like "connectivity" or special buttons i dont want them, so I can really spend money in quality and sound, but when you start looking for that kind of headphones they always use it with an amp (I know nothing about sound system), so I was surprised that my motherboard supports 600oms, kind of enough for any mid-range studio headphones!


The funny part of everything, my laptop start working normally, what happened, idk, but for sure I'm going to stop using it a my main war machine.
For sure, I'm going to document here how I'm dealing using the same repo for different machine.
