---
title: "Unix Philosophy - TENET 1."
date: "2022-Mar-10"
excerpt: "Small is beautiful."
cover_image: "https://catonmat.net/images/100-favorite-books/book-the-unix-philosophy.jpg"
tags: ["unix"]
author: "Chat GPT-3"
finished: true
---

# [TENET](https://dictionary.cambridge.org/dictionary/english/tenet) 1 - Small is beautiful.

> Resist the temptation to turn it into a monolith (your software). Strive for simplicity.

### The Unix Philosophy

Unix is an operating system that is based on a set of principles and philosophies that were developed by its creators,
Ken Thompson and Dennis Ritchie, in the 1970s. These principles are commonly referred to as the "Unix philosophy," and
they are still relevant today in the development of modern software systems.

The first tenet of the Unix philosophy is "Write programs that do one thing and do it well." This principle emphasizes
the importance of creating small, focused programs that perform a single task efficiently and effectively. This approach
is in contrast to the monolithic software systems that were prevalent at the time, which were large and complex programs
that tried to do everything at once.

The idea behind this principle is that small programs are easier to design, test, and maintain than large ones. By
focusing on a single task, a program can be optimized to do that task exceptionally well. This also means that if there
is a problem with one program, it is easier to identify and fix without affecting other programs in the system.

Another benefit of this approach is that small programs can be combined to create more complex systems. Unix achieves
this through the use of pipes, which allow the output of one program to be used as the input of another. This means that
a set of small programs can be combined in various ways to create a customized system that meets the specific needs of
the user.

The Unix philosophy has influenced the development of many modern software systems. One example is the design of web
applications, where the use of microservices has become popular. Microservices are small, independent programs that
provide specific functions and can be combined to create more complex systems. This approach allows developers to create
scalable and flexible applications that can be adapted to changing requirements.

Another example is the development of containerization technologies such as Docker. Containers are lightweight,
standalone packages that contain all the necessary dependencies and configuration needed to run an application. This
approach is similar to the Unix philosophy in that it emphasizes the creation of small, focused programs that can be
combined to create more complex systems.

The Unix philosophy has also influenced the development of software engineering practices such as Agile and DevOps.
These approaches emphasize the importance of iterative development and continuous delivery, which are based on the idea
of creating small, focused programs that can be quickly tested and deployed. This approach allows developers to respond
to changing requirements and deliver software more efficiently.

In conclusion, the first tenet of the Unix philosophy is an important principle that emphasizes the creation of small,
focused programs that perform a single task efficiently and effectively. This approach has influenced the development of
many modern software systems and engineering practices, including microservices, containerization, Agile, and DevOps. By
following this principle, developers can create scalable, flexible, and maintainable software systems that can adapt to
changing requirements.
