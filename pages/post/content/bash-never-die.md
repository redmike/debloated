---
title: "Bash scripting - Scrapping the web."
date: "2022-Jan-19"
excerpt: "Use your command line skills for scrap and rice your terminal."
cover_image: "/static/postimages/terminal.png"
tags: ["bash", "sed"]
author: "Miguel R."
finished: true
---
### pipeline art

I use linux (btw -> ahaha), so kind of being the most minimalist and efficient version of me is live in the terminal.
So in terminal you can really really being efficient if you know the right commands, also when a pipeline command works as expected kind of you feel more smart.

I scraped a database, put it json format, then push it to my database.
The scrap in python, the push in **bash** (for loop plus curl)

soooooo, I started to make small scrapes in bash.
ricing my terminal, I made this
![screenshot of my terminal](/static/postimages/bashscrap/terminaltoday.png)
focus in the gray text.
scrap from zenquotes.io wft happened today long time ago, that day in 0250 "[Pope Fabian](https://www.reginamag.com/saint-fabian-pope-and-martyr/) is martyred during the [Decian persecution.](https://en.wikipedia.org/wiki/Decian_persecution)" sorry dear fabian.

Algorithm:
1 get the content
2 scrap and get the information
3 write to a file
4 read the file in the terminal everytime you open it
5 further improvement

The first and the easiest, get the content
`curl -s https://today.zenquotes.io`
> why -s? we don't get the transfer bar and errors

if you dive with devtools in the page, you can check that the target phrase is under

![dev tools](/static/postimages/bashscrap/inspection.png)
`<h2 id="api-data-1">`

we need to find a specific and exclusive attribute for finding the phrase in a correct way.
so pipeline the id it with grep

`curl -s https://today.zenquotes.io | grep 'id="api-data-1"'`
> as convention, when you write html code, and you want to identify and unique element in the DOM, you use id, so we are thrusting the [guy](https://external-content.duckduckgo.com/iu/?u=http%3A%2F%2F1.bp.blogspot.com%2F-qppMLeabPsM%2FTmSG8_xtmZI%2FAAAAAAAAAy8%2Fl-usBmDdu2s%2Fs1600%2FFunny%2Bpeople%2Bpics%2Bfat%2Bguy.jpg&f=1&nofb=1) who built this

Now we are getting just the element fragment with the quote.
`<h2 id="api-data-1">On This Day in 314 &#8211; Pope Sylvester I is consecrated, as successor to the late Pope Miltiades.&#91;1&#93;</h2>`

You know we need get just the phare, now all the raw code, so how.
well, now can vary in a lot of solutions, I continue using grep and sed, for sure someone with more experience will take this step further and make it smaller.
let's select just the test between ><

`curl -s https://today.zenquotes.io | grep 'id="api-data-1"' | grep -o '>.*<'`
> why -o? so grep return just the matched part
> the dot means match almost everything, and the asterisk should be 0 or more coincidences

what we get?
`>On This Day in 314 &#8211; Pope Sylvester I is consecrated, as successor to the late Pope Miltiades.&#91;1&#93;<`

So the we don't want the >< symbols neither &#stuffs, we can substitute it with sed.

`curl -s https://today.zenquotes.io | grep 'id="api-data-1"' | grep -o '>.*<' | sed -e 's/\&#[0-9]*/''/g'`
> here basically we are telling to sed, substitute (or replace) &#whatever for '' (empty string), the g means don't stop if you find the first occurrence.
> if you feeling weak in sed, check my sed post.

what we get?
`>On This Day in 314 ; Pope Sylvester I is consecrated, as successor to the late Pope Miltiades.;1;<`
from here you can continue trimming the phrase or just leave like that.

I'm going to continue trimming without much effort neither think much about it
`curl -s https://today.zenquotes.io | grep 'id="api-data-1"' | grep -o '>.*<' | sed -e 's/\&#[0-9]*/''/g' -e 's/>/''/g' -e 's/</''/g'`
> bye bigger and smaller simbols


`curl -s https://today.zenquotes.io | grep 'id="api-data-1"' | grep -o '>.*<' | sed -e 's/\&#[0-9]*/''/g' -e 's/>/''/g' -e 's/</''/g' -e 's/;[0-9]*;/''/g'`
> bye ;any number here;


`curl -s https://today.zenquotes.io | grep 'id="api-data-1"' | grep -o '>.*<' | sed -e 's/\&#[0-9]*/''/g' -e 's/>/''/g' -e 's/</''/g' -e 's/;[0-9]*;/''/g' -e 's/\s;/,/'`
> change the space and ; for just a comma

Final result
`On This Day in 314, Pope Sylvester I is consecrated, as successor to the late Pope Miltiades.`

that was the scrapping part, now that we get the phrase the solution that I adopted was write the phrase to a file and then just everytime you open  terminal, it eco the content of the file in the terminal, simple.
so one thing we can do is create a file in your cache and read it from there.
let's add a variable with that
`CACHE_QUOTE="$HOME/.cache/todayquote"`

In my configuration, I use zsh, so go to your zshrc (default ./zshrc)
you can use cat, head or < for see the content of a file in the terminal, I think for this propose use <, so in .zshrc
`< $HOME/.cache/todayquote`
or if your system is *tidy*
`< $XDG_CACHE_HOME/todayquote`
and the script with cron, initrc or whatever you like

What I did?
I continue developing the script and put an put an if statement seeing if the phrase was gather today, so I don't make a curl request when I don't need it.


`CACHE_QUOTE="$HOME/.cache/todayquote"
if [[ $(date +"%D") != $(tail -n 1 $CACHE_QUOTE) ]]; then
	truncate -s 0 $CACHE_QUOTE
	curl -s https://today.zenquotes.io | grep 'id="api-data-1"' | grep -o '>.*<' | sed -e 's/\&#[0-9]*/''/g' -e 's/>/''/g' -e 's/</''/g' -e 's/;[0-9]*;/''/g' -e 's/\s;/,/'> $CACHE_QUOTE
	date +"%D" >> $CACHE_QUOTE
fi`

![finished script](/static/postimages/bashscrap/today.bash.png)

So it looks in the last line for the date, %D means the day without hours, its a daily quote.
If doesn't find todays date it erase the file, gather the quote and append it to the cache file, then append the date to it.

and in my zshrc I execute this script and then echo the quote.


**BASH never die!!**
