---
title: "From video to gif."
date: "2022-Mar-09"
excerpt: "Use your command line skills for scrap and rice your terminal."
cover_image: "/static/postimages/mpvtogif/hero.jpg"
tags: ["short"]
author: "Miguel R."
finished: true
---
with this workflow you will make gif from a video in, I don't know, 10 seconds?
![going cold](/static/postimages/mpvtogif/coldpond.gif)
With two utilities
- mpv.
- imagemagick.

This is the workflow:
1. while you are seeing a video in mpv, you can press `s` and it will make a screenshot of the video and store it 
(default) in $HOME. 
![screenshot of screenshots](/static/postimages/mpvtogif/screenshotofscreenshots.png)
2. as you can see, every picture is 1.8M, a lot.
as you can see, the good thing here is mpv save it in order, this will be handy with imagemagick
3. with imagemagick, just use the convert param and enjoy the result.
for this example, im using `convert -resize 25% *.jpg && convert -delay 100 -loop 0 -dispose previous *.jpg coldpond.gif`
first command -> resize all the images (self-explanatory)
second command -> actually make the gift
> delay 1sec (the time between frame).
> 
> loop 0 (means infinite, if you put 5, it will do 5 loop)
>
> *.jpg (take all the jpg in the folder)
>
> coldpond.gif (the file that will create)
> 
> dispose previous (the image don't stack on top)

**TIP**
I suggest erase the first screenshot, if you make the command, the 0 index will be between nine and ten, be aware of that.
