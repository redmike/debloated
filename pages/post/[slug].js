import matter from 'gray-matter';

import hljs from 'highlight.js';
import bash from 'highlight.js/lib/languages/bash';
import javascript from 'highlight.js/lib/languages/javascript';
import rust from 'highlight.js/lib/languages/rust';
import shell from 'highlight.js/lib/languages/shell';
import typescript from 'highlight.js/lib/languages/typescript';
import 'highlight.js/styles/vs.css';
import Head from 'next/head.js';
import * as fs from 'node:fs';
import * as path from 'node:path';
import showdown from 'showdown';
import showdownHighlight from 'showdown-highlight';
import styled, {keyframes} from 'styled-components';
import Header from '../../components/header.js';
import {GlassContainer, lightColor, pageTitle} from '../../styles/globalVariables.js';
// import Image from "next/image";
hljs.registerLanguage('javascript', javascript);
hljs.registerLanguage('bash', bash);
hljs.registerLanguage('shell', shell);
hljs.registerLanguage('typescript', typescript);
hljs.registerLanguage('rust', rust);


const PostContainer = styled.div`
  margin: 1rem;
  display: flex;
  flex-direction: column;
  align-items: center;
  color: ${lightColor};
  padding: 3rem;
  text-align: justify;
  @media (max-width: 460px) {
    padding: 0.5rem;
  }

  img {
    box-shadow: 0 0 7px black;
    border-radius: 5px;
    object-fit: fill;
  }
`;

const Container = styled.div`
	min-height: 100vh;
	background: url('/static/backgrounds/mesh-gradient-redcircle.png') no-repeat
		50% fixed;
	background-size: cover;
	display: flex;
	align-items: center;
	width: 100vw !important;
	flex-direction: column;
	justify-content: center;
	//margin-top: 3rem;
  padding: 2.5rem 0;
`;

const Title = styled.h1`
	padding: 1rem 0;
	text-decoration: underline;
`;
const blinkingAnimation = keyframes`
	0% {
		opacity: 0;
	}
`
const Content = styled.div`
  span.highlight {
    color: black;
    background: cyan;
    font-weight: bold;
  }

  padding: 3rem;

  * {
    margin: 1rem 0;
  }

  img {
    max-width: 100%;
    border-radius: 4px;
    box-shadow: 0 0 5px 1px black;
  }

  code {
		padding: 0.25rem 0;
		color: hsl(90 25% 50%);
		background-color: black;
		width: 100%;
    &:after {
	    font-weight: bold;
      content: ' _';
	    animation: ${blinkingAnimation} 1.5s steps(2) infinite;
    }
	}
	blockquote {
		display: flex;
		color: yellow;
		background-color: black;
		padding: 0.5rem 1rem;
		border-radius: 5px;
		box-shadow: 0 0 7px black;
		font-family: monospace;
		br {
			height: 0;
			margin: 0 0 1rem 0;
		}
    //* {
    //  &:before {
    //    content: ">|";
    //    position: absolute;
    //    transform: translateX(-20px);
    //    color: dodgerblue;
    //    height: 100%;
    //  }
    //}


	}

	@media (max-width: 460px) {
		max-width: 350px;
		padding: 1rem;
	}
`;

const Post = ({metadata, content, slug}) => {
	showdown.setFlavor('github');
	const converter = new showdown.Converter({
		                                         extensions        : [
			                                         showdownHighlight({
				                                                           pre: true
			                                                           })
		                                         ],
		                                         simplifiedAutoLink: true
	                                         });
	const customProcessContent = (content) => {
		return content.replace(/==([\w\s,*_]+)==/g, "<span class='highlight'>&nbsp;$1&nbsp;</span>");
	};
	return (
		<Container>
			<Head>
				<title>
					{pageTitle} - {metadata.title}
				</title>
			</Head>
			<Header/>
			<GlassContainer id="post" style={{margin: '2.5rem 1rem', maxWidth: '1200px'}}>
				<PostContainer>
					<img
					src={metadata.cover_image}
					alt={metadata.title}
					style={{width: '100%'}}/>
					{/*<Image src={metadata.cover_image} alt={metadata.title} layout='fill' loader/>*/}
					<Title>{metadata.title}</Title>
					<Content
						dangerouslySetInnerHTML={{__html: converter.makeHtml(customProcessContent(content))}}
					/>
				</PostContainer>
			</GlassContainer>
		</Container>
	);
};

export default Post;

export const getStaticPaths = async () => {
	const files = fs.readdirSync(path.join('pages/post/content'));
	const paths = files.map((filename) => ({
		params: {
			slug: filename.replace('.md', ''),
		},
	}));
	return {
		paths,
		fallback: false,
	};
};

export const getStaticProps = async ({params: {slug}}) => {
	const markDownWithMeta = fs.readFileSync(
		path.join('pages/post/content', slug + '.md'),
		'utf-8',
	);
	const {data: metadata, content} = matter(markDownWithMeta);
	return {
		props: {
			metadata,
			content,
			slug,
		},
	};
};
