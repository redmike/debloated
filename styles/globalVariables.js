import styled from "styled-components";

export const darkColor = '#222222';
export const darkPurple = '#420699';
export const purple = '#8777ff';
export const prussianBlue = '#003153';
export const ceruleanBlue = '#2A52BE';
export const castletonGreen = '#00563F';
export const lightColor = '#F1DED4';
export const pageTitle = 'Debloat US';

export const GlassContainer = styled.div`
  background-color: rgba(255, 255, 255, .15);
  backdrop-filter: blur(15px);
  border-radius: 5px;
  padding: 1rem 0;
  margin-top: 4rem;
  transition: background-color .3s linear;
  @media (min-width: 768px) {
    padding: 1rem;
  }
  @media (max-width: 460px) {
    width: 90vw;
  }

  &:hover {
    background-color: rgba(255, 255, 255, .35);
    box-shadow: 0 0 0 3px ${darkPurple} inset;
  }
`;

export const MainContainer = styled.div`
  background: url("/static/backgrounds/mesh-gradient.png") no-repeat 50% fixed;
  background-size: cover;
  min-height: 100vh;
  padding: 4rem 0;
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`

/*
    Breakpoints:
    ------------
    sm: min-width: 640px
    md: min-width: 768px
    lg: min-width: 1024px
    xl: min-width: 1280px
    2xl: min-width: 1536px
 */
